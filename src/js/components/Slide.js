import React from "react";
import classnames from "classnames";

export default React.createClass({

  displayName: "Slide",

  render() {
    var classes = classnames("slide", "highlightable-component", this.props.className);
    return (
      <div className={classes}>
        {this.props.children}
      </div>
    );
  }
});