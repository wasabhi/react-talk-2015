import SelectableList from "components/SelectableList";
import React from "react";

export default React.createClass({

  displayName: "React Sections",

  getDefaultProps() {
    return {
      sections: ["Baked in optimizations", "It's easy to understand"],
      selected: "Baked in optimizations"
    }
  },

  render() {
    return <SelectableList {...this.props} />
  }
});