import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"

var code = `React.createClass({
  render() {
    return React.createElement('div', null, 'Some Text');
  }
});`;

export default React.createClass({

  displayName: "Virtual DOM",

  render() {
    return (
      <ContentSlide title="The Virtual DOM" {...this.props}>
        <p>is just javascript</p>
        <CodeSnippet>{code}</CodeSnippet>
      </ContentSlide>
    );
  }
});