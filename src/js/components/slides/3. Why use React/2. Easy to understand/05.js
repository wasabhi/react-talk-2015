import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";
import CodeSnippet from "components/CodeSnippet"
import PropsAndStateDemo from "components/demos/PropsAndStateDemo"
import Split from "components/Split"

var code = `React.createClass({
  _onInput(e) {
    this.setState({
      text: e.target.value
    });
  },
  render() {
    return (
      <div>
        <input type="text"
               value={this.state.text}
               onInput={this._onInput} />
        <PropsDemo text={this.state.text} />
      </div>
    );
  }
});`;

export default React.createClass({

  displayName: "State",

  getInitialState() {
    return {
      text: "Original text"
    }
  },

  _onInput(e) {
    this.setState({
      text: e.target.value
    });
  },

  render() {
    return (
      <ContentSlide title="State" {...this.props}>
        <Split>
          <CodeSnippet>{code}</CodeSnippet>
          <PropsAndStateDemo />
        </Split>
      </ContentSlide>
    );
  }
});