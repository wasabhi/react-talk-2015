import React from "react";
import ContentSlide from "components/slide-types/ContentSlide";

export default React.createClass({

  displayName: "Flux - Dispatchers",

  render() {
    return (
      <ContentSlide title="Dispatchers" {...this.props}>
        <img src="/img/flux-simple-f8-diagram-1300w.png" width="650" />
        <p>An event emitter</p>
        <p>Only one event at a time</p>
      </ContentSlide>
    );
  }
});