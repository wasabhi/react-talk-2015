import BulletListSlide from "components/slide-types/BulletListSlide";
import ReactSections from "components/ReactSections";
import React from "react";

export default React.createClass({

  displayName: "What is React?",

  render() {
    return (
      <BulletListSlide title="What is React?" {...this.props}>
        <p>The V in MVC</p>
        <p>It's efficient, not fast</p>
        <p>Not a framework</p>
        <p>Component-based</p>
      </BulletListSlide>
    );
  }
});