import ControlActions from "actions/ControlActions";
import React from "react";

export default React.createClass({

  displayName: "Controls",

  getDefaultProps() {
    return {
      slide: 1,
      slideCount: 1
    }
  },

  componentDidMount() {
    document.addEventListener("keydown", this.onKeyDown);
  },

  componentWillUnmount() {
    document.removeEventListener("keydown", this.onKeyDown);
  },

  onKeyDown(e) {
    if (!e.metaKey) {
      switch (e.which) {
        case 39:
        case 40:
          this.onNext();
          break;

        case 37:
        case 38:
          this.onPrevious();
          break;

        case 67:
          this.onToggleContrast();
          break;

        case 72:
          this.onToggleHighlightComponents();
          break;
      }
    }
  },

  onNext() {
    ControlActions.nextSlide();
  },

  onPrevious() {
    ControlActions.previousSlide();
  },

  onToggleContrast() {
    ControlActions.toggleContrast();
  },

  onToggleHighlightComponents() {
    ControlActions.toggleHighlightComponents();
  },

  _getButton(className) {
    return {__html: "<svg class='" + className + "'><use xlink:href='#" + className + "'></use>"};
  },

  _getPreviousButton() {
    if (this.props.slide > 1) {
      return <button onClick={this.onPrevious} dangerouslySetInnerHTML={this._getButton("icon-arrow-circle-left")} />;
    }
  },

  _getNextButton() {
    if (this.props.slide < this.props.slideCount) {
      return <button onClick={this.onNext} dangerouslySetInnerHTML={this._getButton("icon-arrow-circle-right")} />;
    }
  },

  render() {

    var previousButton = this._getPreviousButton();
    var nextButton = this._getNextButton();

    return (
      <div className="controls highlightable-component">
        {previousButton}
        {nextButton}
      </div>
    );
  }
});