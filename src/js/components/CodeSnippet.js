import React from "react";
import hljs from "highlight.js"

export default React.createClass({

  displayName: "Code Snippet",

  componentDidMount() {
    hljs.highlightBlock(React.findDOMNode(this.refs.code));
  },

  render() {
    return (
      <pre ref="code" className="javascript" {...this.props}>
        <code className="github">{this.props.children}</code>
      </pre>
    );
  }
});