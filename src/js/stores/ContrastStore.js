import ImmutableStore from "stores/ImmutableStore";
import PresentationDispatcher from "dispatcher/PresentationDispatcher";
import PresentationConstants from "constants/PresentationConstants";
import Slides from "components/Slides";

class ContrastStore extends ImmutableStore {

  getInitialData() {
    return {
      highContrast: true
    }
  }

  registerListeners() {
    PresentationDispatcher.register(action => {
      switch (action.type) {
        case PresentationConstants.ActionTypes.TOGGLE_CONTRAST:
          this.toggleContrast();
          break;
      }
    });
  }

  toggleContrast() {
    this.set("highContrast", !this.get('highContrast'));
  }
}

export default new ContrastStore();