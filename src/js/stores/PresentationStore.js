import ImmutableStore from "stores/ImmutableStore";
import PresentationDispatcher from "dispatcher/PresentationDispatcher";
import PresentationConstants from "constants/PresentationConstants";
import Slides from "components/Slides";
import React from "react/addons";

class PresentationStore extends ImmutableStore {

  getInitialData() {
    return {
      slide: 1,
      slideCount: Slides.length,
      highContrast: true,
      highlightComponents: false
    }
  }

  registerListeners() {
    PresentationDispatcher.register(action => {
      switch (action.type) {
        case PresentationConstants.ActionTypes.NEXT_SLIDE:
          this.nextSlide();
          break;

        case PresentationConstants.ActionTypes.PREVIOUS_SLIDE:
          this.previousSlide();
          break;

        case PresentationConstants.ActionTypes.TOGGLE_CONTRAST:
          this.toggleContrast();
          break;

        case PresentationConstants.ActionTypes.TOGGLE_HIGHLIGHT_COMPONENTS:
          this.toggleHighlightComponents();
          break;
      }
    });
  }

  getSlide() {
    return this.get("slide");
  }

  getSlideCount() {
    return this.get("slideCount");
  }

  setSlide(i) {
    this.set("slide", i);
  }

  toggleContrast() {
    this.set("highContrast", !this.get('highContrast'));
  }

  toggleHighlightComponents() {
    this.set("highlightComponents", !this.get('highlightComponents'));
  }

  nextSlide() {
    var slide = this.getSlide();
    if (slide < this.getSlideCount()) {
      this.setSlide(slide + 1);
    }
  }

  previousSlide() {
    var slide = this.getSlide();
    if (slide > 1) {
      this.setSlide(slide - 1);
    }
  }
}

export default new PresentationStore();