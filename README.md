# A React presentation #

This presentation on React is built in React by [@edave](https://twitter.com/edave).

Pull requests for additional functionality accepted.

![Screen Shot 2015-05-15 at 1.00.23 am.png](https://bitbucket.org/repo/7enbr7/images/2740895056-Screen%20Shot%202015-05-15%20at%201.00.23%20am.png)

## Running

    $ npm install
    $ gulp

You can then load http://localhost:3000 in your browser